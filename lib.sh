#!/bin/sh

# notify function
function notify {
  echo "$DATE Check $2 return ($3)... $1" >> $LOG
  if [[ $state != $last_state ]]; then
    # pokud se predchozi a soucasny stav lisi, informuji
    mail -s "[BOT] $2 $1" $ADMIN << EOF
Test $2 zkoncil ve stavu $1.
 
 $3

$DATE

---
Monitor Bot
EOF
    if [ $? -eq 0 ]; then 
      echo "$DATE Check $2 send email... OK"
    else
      echo "$DATE Check $2 can't send email... ALERT"
    fi
  fi

}


function simple_check {
    check_name=$(basename ${check})
    last_state=$(tail -n1 ${CHECKS_DATA}/${check_name} | cut -d\; -f2)
    ret=$($check)
    state=$?

    case $state in 
      0) notify "OK" $check_name $ret ;;
      1) notify "ALERT" $check_name $ret ;;
      *) notify "WARN" $check_name $ret ;;
    esac

    # ukladam aktualni stav do tmp souboru
    echo "$DATE;$state;$ret" >> ${CHECKS_DATA}/${check_name}
}

