#!/bin/sh
#
#  Exec by cron every 5 min and run everything ./checks/
#
#
. ./config
. ./lib.sh

for mon in history/*; 
do 
  echo "$(basename $mon)"
  tail -n1 $mon | awk -F\; '{ print "  State:    "$2"\n  Message: "$3"\n  Update:  "$1} ' 
done
